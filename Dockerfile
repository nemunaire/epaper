FROM alpine:3.19

ENV MUSL_LOCPATH="/usr/share/i18n/locales/musl"

ENTRYPOINT ["python", "/srv/e-paper/main.py"]

WORKDIR /srv/e-paper
VOLUME /data

RUN apk add --no-cache musl-locales musl-locales-lang python3 py3-cairosvg py3-icalendar py3-pillow py3-pip py3-pygal tzdata

ADD https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Bold.otf /usr/share/fonts/source-sans/SourceSans3-Bold.otf
ADD https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-It.otf /usr/share/fonts/source-sans/SourceSans3-It.otf
ADD https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Regular.otf /usr/share/fonts/source-sans/SourceSans3-Regular.otf

COPY modules modules
COPY fonts fonts
COPY icons icons
COPY main.py .

WORKDIR /data
